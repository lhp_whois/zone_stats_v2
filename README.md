# zone_stats_v2
This script is to count the number of domains of each tld(top-level-domain) by dates. The script is expected to run on the server which hosts the zone files.


### Requirements
1.python3 (tested on python3.4.3, Ubuntu14.04 LTS)

### Installation
1.Download the package
```
$ git clone git@bitbucket.org:lhp_whois/zone_stats_v2.git
```
2.Install modules
```
$ cd zone_stats_v2
$ pip install -r requirements.txt
$ mv config_ref.py config.py (edit config.py if necessary)
```

### Usage
```
$ python sum_results.py <start_date(yyyy-mm-dd)> <end_date(yyyy-mm-dd)>
```
e.g.
```
$ python sum_results.py 2019-05-01 2019-05-07
```
The data are exported to file output.csv in the current directory (configurable in config.py, set OUTPUT_CSV = "<filename>").
