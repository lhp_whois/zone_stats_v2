import os
import tarfile
import subprocess
import shutil
from config import *

def extractTarFile(tarFile):
    tar = tarfile.open(tarFile)
    tar.extractall(path=EXTRACT_DIR)
    tar.close()
    for filename in os.listdir(EXTRACT_DIR):
        file = os.path.join(EXTRACT_DIR, filename)
        if os.path.isfile(file):
            return file
    return None

def zone_stat(date, tld):
    tld_dir = os.path.join(ZONE_FILE_DIR, date, tld)
    tarFile = os.path.join(TEMP_DIR, tld+'.tar.gz')
    cmd = 'cat {tld_dir}/* > {tarfile}'.format(tld_dir=tld_dir, tarfile=tarFile)
    res = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE).stdout.read()
    file = extractTarFile(tarFile)
    assert file is not None
    count = lines_count(file)
    # rm_cmd = 'rm -r {dir}/*'.format(dir=EXTRACT_DIR)
    # ret = os.system(rm_cmd)
    shutil.rmtree(EXTRACT_DIR)
    os.remove(tarFile)
    return count

def zone_stats(date):
    res = {}
    for tld in os.listdir(os.path.join(ZONE_FILE_DIR, date)):
        res[tld] = zone_stat(date, tld)
    return res

def lines_count(file):
    return sum(1 for line in open(file, 'r'))
