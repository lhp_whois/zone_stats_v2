import os
import sys
import json
import requests
import datetime
import tarfile
import shutil
import traceback
from lxml import html
from requests.auth import HTTPBasicAuth
import logging
LOG_FILENAME = 'error.out'
logging.basicConfig(filename=LOG_FILENAME, level=logging.ERROR)

dateUrl = 'http://bestwhois.org/zone_file/{date}/'
USERNAME = 'hiber'
PASSWORD = 'Qv4GatBRrFtS'
DATA_DIR = '/home/hplu/storage'

def getTLDs(date):
    stats = {}
    url = dateUrl.format(date=date)
    res = requests.get(url, auth=HTTPBasicAuth(USERNAME, PASSWORD))
    tree = html.fromstring(res.content)
    tlds = tree.xpath('//a/text()')[1:]
    tlds = list(map(lambda x: x.strip('/'), tlds))
    return tlds

def extractFiles(url):
    res = requests.get(url, auth=HTTPBasicAuth(USERNAME, PASSWORD))
    tree = html.fromstring(res.content)
    links = tree.xpath('//a')[1:]
    urls = []
    return list(map(lambda x:x.text, links))

def downloadFile(url, path, filename):
    try:
        req = requests.get(url, stream=True, auth=HTTPBasicAuth(USERNAME, PASSWORD))
        file = os.path.join(path, filename)
        with open(file, 'wb+') as f:
            for chunk in req.iter_content(chunk_size=1024):
                if chunk:
                    f.write(chunk)
                    f.flush()
        return True
    except:
        print('download failed')
        return False

def downloadTLD(date, tld):
    print(date, tld)
    dir = os.path.join(DATA_DIR, date, tld)
    if not os.path.exists(dir):
        os.makedirs(dir)

    baseUrl = dateUrl.format(date=date) + tld + '/'
    filenames = extractFiles(baseUrl)
    for filename in filenames:
        url = baseUrl + filename
        status = downloadFile(url, dir, filename)
        if not status:
            print(dir, filename)

def downloadTLDs(date):
    dir = os.path.join(DATA_DIR, date)
    if not os.path.exists(dir):
        os.makedirs(dir)
    tlds = getTLDs(date)
    for tld in tlds:
        downloadTLD(date, tld)

if __name__ == '__main__':
    downloadTLDs(sys.argv[1])
