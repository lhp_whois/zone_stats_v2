import os
import sys
import csv
import json
import datetime
from zone_stats import zone_stats
from config import *

def twoDigits(num):
    if num < 10:
        return '0' + str(num)
    else:
        return str(num)

def str2DateTime(date):
    year, month, day = date.split('-')
    return datetime.datetime(year=int(year), month=int(month), day=int(day))

def dateTime2Str(dateTime):
    return '{year}-{month}-{day}'.format(year=dateTime.year, month=twoDigits(dateTime.month), day=twoDigits(dateTime.day))

if __name__ == "__main__":
    if len(sys.argv) != 3:
        print('python3 sum_results.py <start-date> <end-date>')
        print('e.g. python3 sum_results.py 2018-04-15 2018-04-20')
    if not os.path.exists(RES_DIR):
        os.makedirs(RES_DIR)

    startDate = sys.argv[1]
    endDate = sys.argv[2]
    if int(startDate.replace('-','')) > int(endDate.replace('-', '')):
        print('start date must be no greater than end date!')

    data = {}
    domains = set()
    dates = []

    startDateTime = str2DateTime(startDate)
    endDateTime = str2DateTime(endDate)
    cdt = startDateTime
    while cdt <= endDateTime:
        dates.append(dateTime2Str(cdt))
        cdt = cdt + datetime.timedelta(days=1)

    for date in dates:
        fname = date + '.json'
        file = os.path.join(RES_DIR, fname)
        if not os.path.exists(file):
            res = zone_stats(date)
            with open(file, 'w+') as jf:
                json.dump(res, jf)
        with open(file, 'r') as jf:
            res = json.load(jf)
        data[date] = res
        domains |= res.keys()

    csvData = []
    header = [''] + sorted(dates, key=lambda x: int(x.replace('-', '')))
    csvData.append(header)
    for domain in sorted(domains):
        row = [domain]
        for date in header[1:]:
            row.append(str(data[date].get(domain, 0)))
        csvData.append(row)

    with open(OUTPUT_CSV, 'w+') as csvFile:
        writer = csv.writer(csvFile)
        writer.writerows(csvData)
